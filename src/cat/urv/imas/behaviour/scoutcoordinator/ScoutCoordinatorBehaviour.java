/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.behaviour.scoutcoordinator;

import cat.urv.imas.agent.AgentType;
import cat.urv.imas.agent.ScoutCoordinatorAgent;
import cat.urv.imas.map.Cell;
import cat.urv.imas.onthology.MessageContent;
import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

/**
 *
 * @author Saddam
 */
public class ScoutCoordinatorBehaviour extends Behaviour {
    private int state = 0;
    private Cell buildingsWithGarbage;
    public ScoutCoordinatorBehaviour(ScoutCoordinatorAgent agent){
        super(agent);
    }
    
    @Override
    public void action(){
        switch(state){
            case 0:
                this.buildingsWithGarbage = reciveNewUpdatesFromScouts();
                state = 1;
                break;
            case 1:
                if(this.buildingsWithGarbage != null)
                    sendNewUpdatesToCoordinator();
                state = 0;
                break;
        }
    }
    
    public Cell reciveNewUpdatesFromScouts(){
        ScoutCoordinatorAgent agent = (ScoutCoordinatorAgent)getAgent();
        MessageTemplate mt = MessageTemplate.and(
                MessageTemplate.MatchPerformative(ACLMessage.INFORM), 
                MessageTemplate.MatchConversationId(MessageContent.GET_DETECTED_GARBAGE));
        try{
            ACLMessage msg = agent.blockingReceive(mt);
            agent.log("recevied new garbage");
            if(msg != null)
                return (Cell)msg.getContentObject();
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
    
    public void sendNewUpdatesToCoordinator(){
        ScoutCoordinatorAgent agent = (ScoutCoordinatorAgent)getAgent();
        ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
        msg.setConversationId(MessageContent.GET_DETECTED_GARBAGE);
        msg.clearAllReceiver();
        msg.addReceiver(agent.coordinatorAgent);
        msg.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
        agent.log("new garbage detected");
        try {
            msg.setContentObject(buildingsWithGarbage);
            agent.log("message sent");
            agent.send(msg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public boolean done(){
        return false;
    }
}
