/**
 *  IMAS base code for the practical work.
 *  Copyright (C) 2014 DEIM - URV
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.urv.imas.behaviour.movableagent;

import cat.urv.imas.agent.MovableAgent;
import cat.urv.imas.agent.ScoutAgent;
import cat.urv.imas.map.Cell;
import cat.urv.imas.onthology.Direction;
import cat.urv.imas.onthology.MessageContent;
import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

/**
 *
 * @author Fadi
 */
public abstract class MoveBehaviour extends Behaviour {
    protected int state = 0;
    protected int currentSimulationStep;
    public MoveBehaviour(MovableAgent agent){
        super(agent);
    }
    
    /**
     * current agent send a message to system agent that I'm ready to work
     * @return true if the message sent successfully, otherwise return false
     */
    public boolean sendIAmReady(){
        MovableAgent agent = (MovableAgent)getAgent();
        ACLMessage initialRequest = new ACLMessage(ACLMessage.INFORM);
        initialRequest.setConversationId(MessageContent.I_AM_READY);
        initialRequest.clearAllReceiver();
        initialRequest.addReceiver(new AID("system", AID.ISLOCALNAME));
        initialRequest.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
        initialRequest.setPerformative(ACLMessage.INFORM);
        try {
            initialRequest.setContent(MessageContent.I_AM_READY);
            agent.send(initialRequest);
            agent.log("i'm ready");
            return true;
        } catch (Exception e) {
            initialRequest.setPerformative(ACLMessage.FAILURE);
            agent.errorLog(e.toString());
            e.printStackTrace();
        }
        return false;
    }
    
    /**
     * receive current simulation step from system agent to do next step
     * @return integer value which is the current step, or return -1 if some thing wrong happen
     */
    public int receiveSimulationStep(){
        MovableAgent agent = (MovableAgent)getAgent();
        MessageTemplate mt = MessageTemplate.and(
        MessageTemplate.MatchConversationId("simulation step"), 
        MessageTemplate.MatchPerformative(ACLMessage.INFORM));
        ACLMessage msg = agent.blockingReceive(mt);
        if(msg != null)
            return Integer.parseInt(msg.getContent());
        else
            return -1;
    }
    
    public int move(){
        MovableAgent agent = (MovableAgent)getAgent();
        Direction d = agent.checkRight();
        Cell cell = agent.getCell(d);
        if(agent.canMove(cell))
            agent.getInfoAgent().setDirection(d);
        if(MovableAgent.move(agent)){
            agent.savePath(agent.getAgentCell());
            state = 1;
        }
        else {
            while(!agent.canMove())
                agent.getInfoAgent().setDirection(agent.nextDir(agent.getInfoAgent().getDirection()));
            MovableAgent.move(agent);
            state = 1;
        }
        return state;
    }
}
