/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.behaviour.harvestorcoordinator;

import cat.urv.imas.agent.HarvesterCoordinatorAgent;
import cat.urv.imas.map.BuildingCell;
import cat.urv.imas.map.Cell;
import cat.urv.imas.onthology.GarbageType;
import cat.urv.imas.onthology.MessageContent;
import jade.core.behaviours.Behaviour;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.util.Map;

/**
 *
 * @author Saddam
 */
public class HarvesterCoordinatorBehaviour extends Behaviour {
    private int state = 0;
    private Cell buildingsWithGarbage;
    public HarvesterCoordinatorBehaviour(HarvesterCoordinatorAgent agent){
        super(agent);
    }
    
    @Override
    public void action(){
        switch(state){
            case 0:
                this.buildingsWithGarbage = reciveNewUpdatesFromCoordinator();
                state = 1;
                break;
            case 1:
                if(this.buildingsWithGarbage != null)
                    sendNewUpdatesToHarvesterSubCoordinator();
                state = 0;
                break;
        }
    }
    
    public Cell reciveNewUpdatesFromCoordinator(){
        HarvesterCoordinatorAgent agent = (HarvesterCoordinatorAgent)getAgent();
        MessageTemplate mt = MessageTemplate.and(
               MessageTemplate.MatchPerformative(ACLMessage.INFORM), 
               MessageTemplate.MatchConversationId(MessageContent.GET_DETECTED_GARBAGE));
        try{
            ACLMessage msg = agent.blockingReceive(mt);
            agent.log("recevied new garbage");
            if(msg != null)
                return (Cell)msg.getContentObject();
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
    
    public void sendNewUpdatesToHarvesterSubCoordinator(){
        HarvesterCoordinatorAgent agent = (HarvesterCoordinatorAgent)getAgent();
        ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
        msg.setConversationId(MessageContent.GET_DETECTED_GARBAGE);
        msg.clearAllReceiver();
        BuildingCell cell = ((BuildingCell)this.buildingsWithGarbage);
        Map<GarbageType, Integer> g = cell.getGarbage();
        for (Map.Entry<GarbageType, Integer> entry: g.entrySet()) {
                if (entry.getValue() > 0) {
                    if(entry.getKey() == GarbageType.GLASS)
                        msg.addReceiver(agent.harvesterGlassCoordinatorAgent);
                    else if(entry.getKey() == GarbageType.PAPER)
                        msg.addReceiver(agent.harvesterPaperCoordinatorAgent);
                    else if(entry.getKey() == GarbageType.PLASTIC)
                        msg.addReceiver(agent.harvesterPlasticCoordinatorAgent);
                    break;
                }
            }
        msg.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
        agent.log("new garbage detected");
        try {
            msg.setContentObject(buildingsWithGarbage);
            agent.log("message sent");
            agent.send(msg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public boolean done(){
        return false;
    }
}
