/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.behaviour.harvester;

/**
 *
 * @author Thamer
 */
public enum HarvesterJobType {
    Move(0),
    GotoNearestGarbage(1),
    CollectGarbage(2),
    GoToRecycleCenter(3),
    DropGarbage(4),
    NONE(-1);
    
    private final int value;

    private HarvesterJobType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
