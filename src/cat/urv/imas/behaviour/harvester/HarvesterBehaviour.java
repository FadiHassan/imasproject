/**
 *  IMAS base code for the practical work.
 *  Copyright (C) 2014 DEIM - URV
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.urv.imas.behaviour.harvester;

import cat.urv.imas.agent.HarvesterAgent;
import cat.urv.imas.agent.MovableAgent;
import cat.urv.imas.astar.AStar;
import cat.urv.imas.astar.GraphSearch;
import cat.urv.imas.astar.HarvesterProblem;
import cat.urv.imas.astar.HarvesterState;
import cat.urv.imas.astar.INode;
import cat.urv.imas.astar.IProblem;
import cat.urv.imas.astar.ISearch;
import cat.urv.imas.astar.ISolution;
import cat.urv.imas.astar.Problem;
import cat.urv.imas.behaviour.movableagent.MoveBehaviour;
import cat.urv.imas.map.BuildingCell;
import cat.urv.imas.map.Cell;
import cat.urv.imas.map.RecyclingCenterCell;
import cat.urv.imas.onthology.Direction;
import cat.urv.imas.onthology.GarbageType;
import cat.urv.imas.onthology.HarvesterInfoAgent;
import cat.urv.imas.onthology.MessageContent;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Fadi
 */
public class HarvesterBehaviour extends MoveBehaviour {
    private int current = 1;
    private INode<HarvesterState>[] path;
    private IProblem problem;
    private AStar<HarvesterState> aStar;
    private ISearch graphSearch;
    private ISolution solution;
    protected Cell goal = null;
    protected HarvesterJobType jobType;
    
    public HarvesterBehaviour(HarvesterAgent agent){
        super(agent);
        jobType = HarvesterJobType.NONE;
    }
    
    
    public boolean harvestGarbage(BuildingCell cell){
        HarvesterAgent agent = (HarvesterAgent)getAgent();
        HarvesterInfoAgent info = (HarvesterInfoAgent)agent.getInfoAgent();
        BuildingCell orginalCell = (BuildingCell)agent.getGame().get(cell.getRow(), cell.getCol());
        Map<GarbageType, Integer> garbage = orginalCell.getGarbage();
        int amount = 0;
        GarbageType type = null;
        for(Map.Entry m:garbage.entrySet()){
            if(Integer.parseInt(m.getValue()+"") > 0){
                amount = Integer.parseInt(m.getValue()+"");
                type = (GarbageType)m.getKey();
            }
        } 
        if(info.isAllowdGarbageType(type)){
            orginalCell.removeGarbage(); 
            info.addGarbage(type);
        }
        return !orginalCell.getGarbage().isEmpty();
    }
    
    public Cell[] getRecycleSurronding(int row, int col){
        HarvesterAgent agent  = (HarvesterAgent)getAgent();
        List<Cell> recycles = new ArrayList<Cell>();
        for(int i = row - 1;i <= row + 1;i++)
            for(int j = col - 1;j <= col + 1;j++){
                if(agent.getGame().get(i, j) instanceof RecyclingCenterCell){
                    RecyclingCenterCell c = (RecyclingCenterCell)agent.getGame().get(i, j);
                    recycles.add(c);
                }
            }
        Cell[] b = new Cell[recycles.size()];
        b = recycles.toArray(b);
        return b;
    }
    
    public boolean throwGarbage(){
        if(getAgent() instanceof HarvesterAgent){
            HarvesterAgent agent = (HarvesterAgent)getAgent();
            int i = agent.getAgentCell().getRow();
            int j = agent.getAgentCell().getCol();
            Cell[] cells = getRecycleSurronding(i, j);
            for(int k = 0;k < cells.length;k++){
                RecyclingCenterCell r = ((RecyclingCenterCell)cells[k]);
                HarvesterInfoAgent info = (HarvesterInfoAgent)agent.getInfoAgent();
                if(info.getGarbage() != null && r.isAllowedGarbageType(info.getGarbage().getKey()) && info.getGarbage().getValue() <= info.getCapacity()) {
                    if(info.getGarbage().getValue() > 0) {
                        info.removeGarbage(info.getGarbage().getKey());
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    /*
    @Override
    public int move(){
        MovableAgent agent = (MovableAgent)getAgent();
        Direction d = agent.checkRight();
        Cell cell = agent.getCell(d);
        if(agent.canMove(cell))
            agent.getInfoAgent().setDirection(d);
        if(MovableAgent.move(agent)){
            //agent.savePath(agent.getAgentCell());
            //state = 1;
        }
        else {
            while(!agent.canMove())
                agent.getInfoAgent().setDirection(agent.nextDir(agent.getInfoAgent().getDirection()));
            MovableAgent.move(agent);
            state = 1;
        }
        return state;
    }*/
    
    @Override
    public void action() { 
        HarvesterAgent agent = (HarvesterAgent)getAgent();
        switch(state){
            case 0:
                if(sendIAmReady())
                    state = 1;
                break;
            case 1:
                currentSimulationStep = receiveSimulationStep();
                if(currentSimulationStep == 0){
                    state = 4;
                }
                if(currentSimulationStep != -1){
                    state = 2;
                }
                break;
            case 2:
                if(jobType == HarvesterJobType.NONE){
                    jobType = HarvesterJobType.Move;
                    state = 2;
                }
                else if(jobType == HarvesterJobType.Move) {
                    move();
                    askCoordinatorAboutNearestGarbage();
                    receiveNearestGarbageLocation();
                    if(this.goal != null) {
                        jobType = HarvesterJobType.GotoNearestGarbage;
                        state = 3;
                    }
                    else
                        state = 1;
                }
                else if(jobType == HarvesterJobType.GotoNearestGarbage){
                    if(this.goal.euclideanDistance(agent.getAgentCell()) < 2 || !planMove()){
                        jobType = HarvesterJobType.CollectGarbage;
                    }
                    else
                        state = 1;
                }
                else if(jobType == HarvesterJobType.CollectGarbage){
                    if(this.goal != null && !(this.goal instanceof BuildingCell))
                        javax.swing.JOptionPane.showMessageDialog(null, this.goal.toString());
                    if(!harvestGarbage((BuildingCell)this.goal)) {
                        jobType = HarvesterJobType.GoToRecycleCenter;
                        this.goal = agent.getGame().get(0, 1);
                        state = 3;
                    }
                    else
                        state = 1;
                }
                else if(jobType == HarvesterJobType.GoToRecycleCenter){
                    if(this.goal.euclideanDistance(agent.getAgentCell()) < 2 || !planMove()){
                        jobType = HarvesterJobType.DropGarbage;
                        state = 2;
                    }
                    else
                        state = 1;
                }
                else if(jobType == HarvesterJobType.DropGarbage){
                    if(!throwGarbage()) {
                        jobType = HarvesterJobType.Move;
                        state = 2;
                    }
                    else
                        state = 1;
                }
                break;
            case 3:
                calculatePathToGoal();
                state = 1;
                break;
            case 4:
                break;
        }
    }
    
    public boolean planMove(){
        HarvesterAgent agent = (HarvesterAgent)getAgent();
        if(path != null && current < path.length){
            Direction d = Direction.values()[path[current].getAction()];
            agent.getInfoAgent().setDirection(d);
            if(agent.canMove() && MovableAgent.move(agent)) {
               current++;
               state = 1;
            }
            else {
                move();
                calculatePathToGoal();
            }
            return true;
        }
        return false;
    }
    
    public int calculatePathToGoal(){
        HarvesterAgent agent = (HarvesterAgent)getAgent();
        problem = new Problem(new HarvesterProblem(new HarvesterState(this.goal), agent, true));
        problem.setInitialState(new HarvesterState(agent.getAgentCell()));
        aStar = new AStar<HarvesterState>(problem);
        graphSearch = new GraphSearch();
        solution = graphSearch.searchOne(aStar);
        if(solution != null)
            path = solution.getGoalPath();
        current = 1;
        return 1;
    }
    
    public int askCoordinatorAboutNearestGarbage(){
        HarvesterAgent agent = (HarvesterAgent)getAgent();
        ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
        msg.setConversationId(MessageContent.GET_NEAREST_GARBAGE);
        msg.clearAllReceiver();
        HarvesterInfoAgent info = (HarvesterInfoAgent)agent.getInfoAgent();
        if(info.isAllowdGarbageType(GarbageType.GLASS))
            msg.addReceiver(agent.harvesterGlassCoordinatorAgent);
        if(info.isAllowdGarbageType(GarbageType.PAPER))
            msg.addReceiver(agent.harvesterPaperCoordinatorAgent);
        if(info.isAllowdGarbageType(GarbageType.PLASTIC))
            msg.addReceiver(agent.harvesterPlasticCoordinatorAgent);
        msg.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
        agent.log("ask the sub-coordinators about nearest garbage");
        try {
            agent.send(msg);
            agent.log("message sent");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 2;
    }
    
    public int receiveNearestGarbageLocation(){
        HarvesterAgent agent = (HarvesterAgent)getAgent();
        MessageTemplate mt = MessageTemplate.and(
               MessageTemplate.MatchPerformative(ACLMessage.INFORM), 
               MessageTemplate.MatchConversationId(MessageContent.GET_NEAREST_GARBAGE));
        
        try{
            ACLMessage msg = agent.receive(mt);
            if(msg != null){
                this.goal = (Cell)msg.getContentObject();
                agent.log("recevied new garbage");
            }
        }catch(Exception e){
            e.printStackTrace();
            return 2;
        }
        return 2;
    }
    
    @Override
    public boolean done() {
        return false;
    }
}
