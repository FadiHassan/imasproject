/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.behaviour.harvestersubcoordinator;

import cat.urv.imas.agent.HarvesterSubCoordinatorAgent;
import cat.urv.imas.map.BuildingCell;
import cat.urv.imas.map.Cell;
import cat.urv.imas.onthology.GarbageType;
import cat.urv.imas.onthology.MessageContent;
import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Saddam
 */
public class HarvesterSubCoordinatorBehaviour extends Behaviour {
    protected int state = 0;
    protected List<Cell> buildingsWithGarbage;
    List<AID> receivers;
    protected AID[] agents;
    protected GarbageType garbageType;
    protected Map<GarbageType, String> msgs;
    public HarvesterSubCoordinatorBehaviour(HarvesterSubCoordinatorAgent agent, AID[] harvesters, GarbageType type){
        super(agent);
        buildingsWithGarbage = agent.buildingsWithGarbage;
        this.agents = harvesters;
        this.garbageType = type;
        msgs = new HashMap<GarbageType, String>();
        msgs.put(GarbageType.GLASS, MessageContent.GLASS_GARBAGE);
        msgs.put(GarbageType.PAPER, MessageContent.PAPER_GARBAGE);
        msgs.put(GarbageType.PLASTIC, MessageContent.PLASTIC_GARBAGE);
        receivers = new ArrayList<AID>();
    }
    
    public Cell reciveNewUpdatesFromHarvesterCoordinator(){
        HarvesterSubCoordinatorAgent agent = (HarvesterSubCoordinatorAgent)getAgent();
        MessageTemplate mt = MessageTemplate.and(
               MessageTemplate.MatchPerformative(ACLMessage.INFORM), 
               MessageTemplate.MatchConversationId(MessageContent.GET_DETECTED_GARBAGE));
        try{
            ACLMessage msg = agent.receive(mt);
            agent.log("recevied new garbage");
            if(msg != null) {
                return (Cell)msg.getContentObject();
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
    
    public void reciveNewGarbage() {
        HarvesterSubCoordinatorAgent agent = (HarvesterSubCoordinatorAgent)getAgent();
        Cell cell = reciveNewUpdatesFromHarvesterCoordinator();
        if(cell != null){
            agent.log("new garbage added to the buffer" + cell.toString());
            if(!this.buildingsWithGarbage.contains(cell))
                this.buildingsWithGarbage.add(cell);
            else
                agent.log("not added");
        }
    }
    
    public void sendNearestGarbageToHarvester(){
        HarvesterSubCoordinatorAgent agent = (HarvesterSubCoordinatorAgent)getAgent();
        ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
        msg.setConversationId(MessageContent.GET_NEAREST_GARBAGE);
        msg.clearAllReceiver();
        msg.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
        agent.log("new garbage detected");
        try {
            if(buildingsWithGarbage.size() > 0 && receivers.size() > 0){
                msg.setContentObject(buildingsWithGarbage.get(0));
                msg.addReceiver(receivers.get(0));
                agent.send(msg);
                buildingsWithGarbage.remove(0);
                receivers.remove(0);
                agent.log("message sent");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public AID receiveQueryAboutNearestGarbage(){
        HarvesterSubCoordinatorAgent agent = (HarvesterSubCoordinatorAgent)getAgent();
        MessageTemplate mt = MessageTemplate.and(
               MessageTemplate.MatchPerformative(ACLMessage.REQUEST), 
               MessageTemplate.MatchConversationId(MessageContent.GET_NEAREST_GARBAGE));
        try{
            ACLMessage msg = agent.receive(mt);
            agent.log("recevied new garbage");
            if(msg != null) {
                return msg.getSender();
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
    
    @Override
    public void action(){
        switch(state){
            case 0:
                this.reciveNewGarbage();
                AID harv = this.receiveQueryAboutNearestGarbage();
                if(harv != null){
                    receivers.add(harv);
                    this.sendNearestGarbageToHarvester();
                }
                break;
            case 1:
                break;
        }
    }
    
    @Override
    public boolean done(){
        return false;
    }
}
