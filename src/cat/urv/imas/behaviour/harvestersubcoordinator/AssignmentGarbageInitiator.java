/**
 *  IMAS base code for the practical work.
 *  Copyright (C) 2014 DEIM - URV
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.urv.imas.behaviour.harvestersubcoordinator;

import cat.urv.imas.agent.HarvesterSubCoordinatorAgent;
import cat.urv.imas.map.Cell;
import jade.core.AID;
import jade.lang.acl.ACLMessage;
import jade.proto.ContractNetInitiator;
import java.util.List;
import java.util.Vector;

/**
 * Behaviour for the Coordinator agent to deal with AGREE messages.
 * The Coordinator Agent sends a REQUEST for the
 * information of the game settings. The System Agent sends an AGREE and 
 * then it informs of this information which is stored by the Coordinator Agent. 
 * 
 * NOTE: The game is processed by another behaviour that we add after the 
 * INFORM has been processed.
 */
public class AssignmentGarbageInitiator extends ContractNetInitiator {
    private boolean finish = false;
    private String title = "";
    private AID[] agents;
    protected List<Cell> buildingsWithGarbage;
    public AssignmentGarbageInitiator(HarvesterSubCoordinatorAgent agent, ACLMessage msg, AID[] harvesters) {
        super(agent, msg);
        agent.log("Started Contract Net " + agent.getLocalName());
        agents = harvesters;
    }

    protected Vector prepareCfps(ACLMessage cfp) {
        cfp = new ACLMessage(ACLMessage.CFP);
        cfp.setContent(title);
        for (int i = 0; i < agents.length; ++i) {
            cfp.addReceiver(agents[i]);
        }
        Vector v = new Vector();
        v.add(cfp);
        return v;
    }

    protected void handlePropose(ACLMessage propose, Vector acceptances) {
        ACLMessage reply = propose.createReply();
        // Evaluate the proposal
        if (true) {
            reply.setPerformative(ACLMessage.ACCEPT_PROPOSAL);
        }
        else {
            reply.setPerformative(ACLMessage.REJECT_PROPOSAL);
        }
        acceptances.add(reply);
    }

    protected void handleRefuse(ACLMessage refuse) {
        
    }

    protected void handleInform(ACLMessage inform) {
        
    }

    protected void handleAllResponses(Vector responses, Vector acceptances) {
        ACLMessage bestOffer = null;
        int bestPrice = -1;
        for (int i = 0; i < responses.size(); ++i) {
            ACLMessage rsp = (ACLMessage) responses.get(i);
            if (rsp.getPerformative() == ACLMessage.PROPOSE) {
                int price = Integer.parseInt(rsp.getContent());
                if (bestOffer == null || price < bestPrice) {
                    bestOffer = rsp;
                    bestPrice = price;
                }
            }
        }
        if (bestOffer != null) {
            ACLMessage accept = bestOffer.createReply();
            accept.setContent(title);
            acceptances.add(accept);
        }
    }
}
