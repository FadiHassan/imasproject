/**
 *  IMAS base code for the practical work.
 *  Copyright (C) 2014 DEIM - URV
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.urv.imas.behaviour.scout;

import cat.urv.imas.agent.MovableAgent;
import cat.urv.imas.agent.ScoutAgent;
import cat.urv.imas.behaviour.movableagent.MoveBehaviour;
import cat.urv.imas.map.Cell;
import cat.urv.imas.onthology.Direction;
import cat.urv.imas.onthology.MessageContent;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;

/**
 *
 * @author Fadi
 */
public class ScoutBehaviour extends MoveBehaviour {
    
    private Cell[] buildingsWithGarbage = null;
    public ScoutBehaviour(ScoutAgent agent){
        super(agent);
    }
    
    /**
     * Explore the city in current simulation step to detect the garbage 
     * @param currentSimulationStep 
     * @return array of building cell with garbage if there is
     */
    public Cell[] detectGarbage(int currentSimulationStep){
        ScoutAgent agent = (ScoutAgent)getAgent();
        int i = agent.getAgentCell().getRow();
        int j = agent.getAgentCell().getCol();
        Cell[] cells = agent.getGame().detectBuildingsWithGarbage(i, j);
        agent.pgp.setValue(i, j, currentSimulationStep);
        return cells;
    }
    
    @Override
    public int move(){
        ScoutAgent agent = (ScoutAgent)getAgent();
        Direction d = agent.checkRight();
        if(agent.numOfDir() == 4)
            d = agent.pgp.getNextDirection(agent);
        Cell cell = agent.getCell(d);
        if(agent.canMove(cell))
            agent.getInfoAgent().setDirection(d);
        if(MovableAgent.move(agent)){
            agent.savePath(agent.getAgentCell());
            state = 1;
        }
        else {
            while(!agent.canMove())
                agent.getInfoAgent().setDirection(agent.nextDir(agent.getInfoAgent().getDirection()));
            MovableAgent.move(agent);
            state = 1;
        }
        this.buildingsWithGarbage = detectGarbage(currentSimulationStep);
        if(this.buildingsWithGarbage.length > 0)
            state = 3;
        return state;
    }
    
    @Override
    public void action() { 
        ScoutAgent agent = (ScoutAgent)getAgent();
        switch(state){
            case 0:
                if(sendIAmReady())
                    state = 1;
                break;
            case 1:
                currentSimulationStep = receiveSimulationStep();
                if(currentSimulationStep == 0){
                    state = 4;
                }
                if(currentSimulationStep != -1){
                    state = 2;
                }
                break;
            case 2:
                state = move();
                break;
            case 3:
                //send the new detected building with garbage to scout coordinator
                if(this.buildingsWithGarbage != null)
                    sendToScoutCoordinator(this.buildingsWithGarbage);
                state = 1;
                break;
            case 4:
                break;
        }
    }
    
    public void sendToScoutCoordinator(Cell[] buildingsWithGarbage){
        ScoutAgent agent = (ScoutAgent)getAgent();
        ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
        msg.setConversationId(MessageContent.GET_DETECTED_GARBAGE);
        msg.clearAllReceiver();
        msg.addReceiver(agent.scoutCoordinatorAgent);
        msg.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
        agent.log("new garbage detected");
        try {
            for(int i = 0;i < buildingsWithGarbage.length;i++){
                msg.setContentObject(buildingsWithGarbage[i]);
                agent.send(msg);
                agent.log("message sent");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public boolean done() {
        return state == 4;
    }
}
