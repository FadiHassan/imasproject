/**
 *  IMAS base code for the practical work.
 *  Copyright (C) 2014 DEIM - URV
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cat.urv.imas.behaviour.system;

import cat.urv.imas.agent.SystemAgent;
import cat.urv.imas.map.Cell;
import cat.urv.imas.map.SettableBuildingCell;
import cat.urv.imas.onthology.GarbageType;
import cat.urv.imas.onthology.MessageContent;
import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 *
 * @author Fadi
 */
public class SimulationBehaviour extends Behaviour {
    private long perioud = 1000;
    private int state = 0;
    private List<AID> readyAgents;
    private List<SettableBuildingCell> settableBuildingCells;
    private GarbageType[] garbageTypes;
    private Random r;
    private ACLMessage initialRequest;
    private int currentSimulationStep = 0;
    private Map<Integer, Cell> buildingsWithGarbage;
    private Map<Integer, Integer> detectGarbageStatisitc;
    private Map<Integer, Integer> collectGarbageStatistic;
            
    public SimulationBehaviour(SystemAgent agent, long perioud){
        super(agent);
        this.perioud = perioud;
        this.readyAgents = new ArrayList<AID>();
        settableBuildingCells = new ArrayList<SettableBuildingCell>();
        Cell[][] map = agent.getGame().getMap();
        for(int i = 0;i < map.length;i++){
            for(int j = 0;j < map[i].length;j++){
                if(map[i][j] instanceof SettableBuildingCell)
                    settableBuildingCells.add((SettableBuildingCell)map[i][j]);
            }
        }
        r = new Random();
        garbageTypes = new GarbageType[]{GarbageType.GLASS, GarbageType.PAPER, GarbageType.PLASTIC};
        buildingsWithGarbage = new HashMap<Integer, Cell>();
        detectGarbageStatisitc = new HashMap<Integer, Integer>();
        collectGarbageStatistic = new HashMap<Integer, Integer>();
    }
    
    @Override
    public void action() {
        SystemAgent agent = (SystemAgent)this.getAgent();
        switch(state){
            case 0:
                if(checkAllAgentsReady()){
                    initialzitionMessage();
                    this.state = 1;
                }
                break;
            case 1:
                try{
                    Thread.sleep(this.perioud);
                } catch(Exception ex){ }
                this.currentSimulationStep = agent.getGame().getSimulationSteps();
                generateNewGarebage(this.currentSimulationStep);
                agent.updateGUI();
                sendCurrentSimulationStep(currentSimulationStep);
                receiveStatisticFromCoordinator();
                agent.getGame().setSimulationSteps(--this.currentSimulationStep);
                if(this.currentSimulationStep == 0)
                    this.state = 2;
                break;
            case 2:
                agent.log("***************simulation finished*********************");
                showStatisitcs();
                state = 3;
                break;
            case 3:
                break;
        }
    }
    
    /**
     * Initializing the general message for send simulation step for all movable agents 
     */
    public void initialzitionMessage(){
        initialRequest = new ACLMessage(ACLMessage.INFORM);
        initialRequest.setConversationId("simulation step");
        initialRequest.clearAllReceiver();
        for(int i = 0;i < readyAgents.size();i++)
            initialRequest.addReceiver(this.readyAgents.get(i));
        initialRequest.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
        initialRequest.setPerformative(ACLMessage.INFORM);
    }
    
    /**
     * 
     * @return 
     */
    public boolean checkAllAgentsReady(){
        SystemAgent agent = (SystemAgent)getAgent();
        MessageTemplate mt = MessageTemplate.and(
                MessageTemplate.MatchPerformative(ACLMessage.INFORM), 
                MessageTemplate.MatchConversationId(MessageContent.I_AM_READY));
        ACLMessage msg = agent.receive(mt);
        if(msg != null){
            AID sender = msg.getSender();
            agent.log("agent: " + sender.getLocalName() + " arrived");
            readyAgents.add(sender);
        }
        boolean ready = (readyAgents.size() == agent.getMovableAgentsCount());
        
        if(ready)
            agent.log("\n************** all agents ready, simulation start ***************");
        return ready; 
    }
    
    /**
     * send current simulation step to all agents in the system
     * @param simulationStep current simulation step
     */
    public void sendCurrentSimulationStep(int simulationStep){
        SystemAgent agent = (SystemAgent)getAgent();
        agent.log("inform all agents about the current simulation step");
        try {
            initialRequest.setContent(simulationStep+"");
            agent.send(initialRequest);
        } catch (Exception e) {
            initialRequest.setPerformative(ACLMessage.FAILURE);
            agent.errorLog(e.toString());
            e.printStackTrace();
        }
        agent.log("current simulation step sent " + agent.getGame().getSimulationSteps());
    }
    
    /**
     * generate random garbage in the city
     * @param simulationStep current simulation step
     */
    public void generateNewGarebage(int simulationStep){
        SystemAgent agent = (SystemAgent)this.getAgent();
        if(agent.getGame().getMaxNumberBuildingWithNewGargabe() != 0){
            int prop = r.nextInt(100);
            if(prop <= agent.getGame().getNewGarbageProbability() && agent.getGame().getMaxNumberBuildingWithNewGargabe() != 0){
                int x = 0;
                while(!(settableBuildingCells.get(x = r.nextInt(settableBuildingCells.size())).isEmpty()));
                int t = r.nextInt(garbageTypes.length);
                int amount = r.nextInt(agent.getGame().getMaxAmountOfNewGargabe()) + 1;
                settableBuildingCells.get(x).setGarbage(garbageTypes[t], amount);
                agent.getGame().setMaxNumberBuildingWithNewGargabe(agent.getGame().getMaxNumberBuildingWithNewGargabe() - 1);
                agent.log("genereate new garbage at: (" + settableBuildingCells.get(x).getRow() + "," + settableBuildingCells.get(x).getCol() + "), GarbageType: " + garbageTypes[t] + ", and amount:" + amount);
                buildingsWithGarbage.put(simulationStep, settableBuildingCells.get(x));
                detectGarbageStatisitc.put(simulationStep, -1);
            }
        }
    }
    
    public void receiveStatisticFromCoordinator(){
        SystemAgent agent = (SystemAgent)getAgent();
        MessageTemplate mt = MessageTemplate.and(
                MessageTemplate.MatchPerformative(ACLMessage.INFORM), 
                MessageTemplate.MatchConversationId(MessageContent.GET_DETECTED_GARBAGE));
        
        try{
            ACLMessage msg = agent.receive(mt);
            agent.log("recevied new statistic of detected garbage");
            if(msg != null) {
                Cell cell = (Cell)msg.getContentObject();
                Map.Entry<Integer, Cell> lastGenerated = null;
                for(Map.Entry<Integer, Cell> entry : this.buildingsWithGarbage.entrySet())
                    if(entry.getValue().equals(cell))
                        lastGenerated = entry;
                
                this.detectGarbageStatisitc.put(lastGenerated.getKey(), this.currentSimulationStep);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    @Override
    public boolean done() {
        return this.state == 3;
    }
    
    /**
     * calculate time for discovering garbage
     * @return 
     */
    private double averageTimeForDiscoverGarbage(){
        double avg = 0.0;
        int count = 0;
        for(Map.Entry<Integer, Integer> entry : detectGarbageStatisitc.entrySet()){
            count ++;
            avg += entry.getKey() - entry.getValue();
        } 
        return avg / count;
    }
    
    /**
     * calculate ratio of discovered garbage
     * @return 
     */
    private double ratioOfDiscoveredGarbage(){
        return 100.0 * (double)this.detectGarbageStatisitc.size() / (double)this.buildingsWithGarbage.size();
    }

    private void showStatisitcs() {
        SystemAgent agent = (SystemAgent)getAgent();
        agent.gui.showStatistics("\nStatistic: ");
        agent.gui.showStatistics("\nCurrent benefits: ");
        agent.gui.showStatistics("\nAverage time for discovering garbage: " + averageTimeForDiscoverGarbage());
        agent.gui.showStatistics("\nAverage time for collecting garbage: ");
        agent.gui.showStatistics("\nRatio of discovered garbage: " + ratioOfDiscoveredGarbage());
        agent.gui.showStatistics("\nRatio of collected garbage: ");
    }
}
