/**
 *  IMAS base code for the practical work.
 *  Copyright (C) 2014 DEIM - URV
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.urv.imas.agent;

import cat.urv.imas.behaviour.harvester.HarvesterBehaviour;
import jade.core.*;

/**
 * The Harvester agent. 
 * TODO: Agent responsibilities: can bring several types of garbage, 
 * but only a single type of garbage at a time.
 */
public class HarvesterAgent extends MovableAgent {

    /**
     * Harvester Paper Coordinator agent id.
     */
    public AID harvesterPaperCoordinatorAgent;
    /**
     * Harvester Glass Coordinator agent id.
     */
    public AID harvesterGlassCoordinatorAgent;
    /**
     * Harvester Plastic Coordinator agent id.
     */
    public AID harvesterPlasticCoordinatorAgent;
    
    public HarvesterAgent(){
        super(AgentType.HARVESTER);
    }

    /**
     * Agent setup method - called when it first come on-line. Configuration of
     * language to use, ontology and initialization of behaviours.
     */
    @Override
    protected void setup() {
        super.setup();
        
        // search Harvester Paper Coordinator Agent
        this.harvesterPaperCoordinatorAgent = this.search(AgentType.HARVESTER_PAPER_COORDINATOR, null);
        
        // search Harvester Paper Coordinator Agent
        this.harvesterGlassCoordinatorAgent = this.search(AgentType.HARVESTER_GLASS_COORDINATOR, null);
        
        // search Harvester Plastic Coordinator Agent
        this.harvesterPlasticCoordinatorAgent = this.search(AgentType.HARVESTER_PLASTIC_COORDINATOR, null);
        
        this.addBehaviour(new HarvesterBehaviour(this));
    }
}
