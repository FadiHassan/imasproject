/**
 *  IMAS base code for the practical work.
 *  Copyright (C) 2014 DEIM - URV
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.urv.imas.agent;

import cat.urv.imas.behaviour.harvestersubcoordinator.HarvesterSubCoordinatorBehaviour;
import cat.urv.imas.onthology.GarbageType;

/**
 * The Harvester Glass Coordinator agent. 
 * TODO: Coordinating Harvester Agents to harvest paper garbage,
 * and communicate with Coordinator Agent.
 */
public class HarvesterGlassCoordinatorAgent extends HarvesterSubCoordinatorAgent {

    /**
     * Builds the harvester glass coordinator agent.
     */
    public HarvesterGlassCoordinatorAgent() {
        super(AgentType.HARVESTER_GLASS_COORDINATOR, GarbageType.GLASS);
    }

    /**
     * Agent setup method - called when it first come on-line. Configuration of
     * language to use, ontology and initialization of behaviours.
     */
    @Override
    protected void setup() {
        super.setup();
        
    }
    
}
