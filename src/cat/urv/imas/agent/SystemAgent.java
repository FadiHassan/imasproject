/**
 *  IMAS base code for the practical work.
 *  Copyright (C) 2014 DEIM - URV
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.urv.imas.agent;

import cat.urv.imas.onthology.InitialGameSettings;
import cat.urv.imas.onthology.GameSettings;
import cat.urv.imas.gui.GraphicInterface;
import cat.urv.imas.behaviour.system.RequestResponseBehaviour;
import cat.urv.imas.behaviour.system.SimulationBehaviour;
import cat.urv.imas.map.Cell;
import cat.urv.imas.map.StreetCell;
import cat.urv.imas.onthology.HarvesterInfoAgent;
import cat.urv.imas.onthology.ScoutPGP;
import jade.core.*;
import jade.domain.FIPANames.InteractionProtocol;
import jade.lang.acl.*;
import java.util.List;
import java.util.Map;


/**
 * System agent that controls the GUI and loads initial configuration settings.
 * TODO: You have to decide the onthology and protocol when interacting among
 * the Coordinator agent.
 */
public class SystemAgent extends ImasAgent {

    /**
     * GUI with the map, system agent log and statistics.
     */
    public GraphicInterface gui;
    /**
     * Game settings. At the very beginning, it will contain the loaded
     * initial configuration settings.
     */
    private GameSettings game;
    /**
     * The Coordinator agent with which interacts sharing game settings every
     * round.
     */
    private AID coordinatorAgent;
    
    /**
     * Number of movable agent in the city
     */
    private int moveableAgentsCount = 0;

    /**
     * Builds the System agent.
     */
    public SystemAgent() {
        super(AgentType.SYSTEM);
    }

    /**
     * A message is shown in the log area of the GUI, as well as in the 
     * stantard output.
     *
     * @param log String to show
     */
    @Override
    public void log(String log) {
        if (gui != null) {
            gui.log(getLocalName()+ ": " + log + "\n");
        }
        super.log(log);
    }
    
    /**
     * An error message is shown in the log area of the GUI, as well as in the 
     * error output.
     *
     * @param error Error to show
     */
    @Override
    public void errorLog(String error) {
        if (gui != null) {
            gui.log("ERROR: " + getLocalName()+ ": " + error + "\n");
        }
        super.errorLog(error);
    }

    /**
     * Gets the game settings.
     *
     * @return game settings.
     */
    public GameSettings getGame() {
        return this.game;
    }
    
    /**
     * get the gui 
     * @return 
     */
    public GraphicInterface getGui(){
        return this.gui;
    }
    
    /**
     * Agent setup method - called when it first come on-line. Configuration of
     * language to use, ontology and initialization of behaviours.
     */
    @Override
    protected void setup() {

        /* ** Very Important Line (VIL) ************************************* */
        this.setEnabledO2ACommunication(true, 1);

        // 1. Register the agent to the DF
        this.register();
        
        // 2. Load game settings.
        this.game = InitialGameSettings.load("game.settings");
        log("Initial configuration settings loaded");

        // 3. Load GUI
        try {
            this.gui = new GraphicInterface(game);
            gui.setVisible(true);
            log("GUI loaded");
        } catch (Exception e) {
            e.printStackTrace();
        }

        // search CoordinatorAgent
        this.coordinatorAgent = this.search(AgentType.COORDINATOR, null);
        
        //create PGP
        ScoutPGP pgp = new ScoutPGP(this.game.getMap(), this.game.getSimulationSteps());
        
        //4. Generate All Agents
        Object[] arguments = new Object[]{ this.game, pgp };
        //create harvester coordinator agent
        UtilsAgents.createAgent(this.getContainerController(),"harvecoord", "cat.urv.imas.agent.HarvesterCoordinatorAgent", arguments);
        //create harvester paper coordinator agent
        UtilsAgents.createAgent(this.getContainerController(),"papercoord", "cat.urv.imas.agent.HarvesterPaperCoordinatorAgent", arguments);
        //create harvester glass coordinator agent 
        UtilsAgents.createAgent(this.getContainerController(),"glasscoord", "cat.urv.imas.agent.HarvesterGlassCoordinatorAgent", arguments);
        //create harvester plastic coordinator agent
        UtilsAgents.createAgent(this.getContainerController(),"plasticcoord", "cat.urv.imas.agent.HarvesterPlasticCoordinatorAgent", arguments);
        //create scout coordinator agent
        UtilsAgents.createAgent(this.getContainerController(),"scoutcoord", "cat.urv.imas.agent.ScoutCoordinatorAgent", arguments);
        //create scouts and harvesters
        int hcount = 1;
        int scount = 1;
        Map<AgentType, List<Cell>> agents = this.game.getAgentList();
        //GarbageType[][] garbages = this.game.getAllowedGarbageTypePerHarvester();
        //int harvesterCapasity = this.game.getHarvestersCapacity();
        
        for(int i = 0;i < agents.get(AgentType.SCOUT).size();i++){
            StreetCell cell = (StreetCell)agents.get(AgentType.SCOUT).get(i);
            arguments = new Object[]{this.game, cell.getAgent(), cell, pgp};
            String agentName = "scout"+(scount++);
            UtilsAgents.createAgent(this.getContainerController(), agentName, "cat.urv.imas.agent.ScoutAgent", arguments);
        }
        
        for(int i = 0;i < agents.get(AgentType.HARVESTER).size();i++){
            StreetCell cell = (StreetCell)agents.get(AgentType.HARVESTER).get(i);
            arguments = new Object[]{this.game, ((HarvesterInfoAgent)cell.getAgent()), cell};
            String agentName = "harvester"+(hcount++);
            UtilsAgents.createAgent(this.getContainerController(), agentName, "cat.urv.imas.agent.HarvesterAgent", arguments);
        }
        
        this.moveableAgentsCount = agents.get(AgentType.SCOUT).size() + agents.get(AgentType.HARVESTER).size();
        //this.moveableAgentsCount = 8;
        // add behaviours
        // we wait for the initialization of the game
        MessageTemplate mt = MessageTemplate.and(MessageTemplate.MatchProtocol(InteractionProtocol.FIPA_REQUEST), MessageTemplate.MatchPerformative(ACLMessage.REQUEST));

        this.addBehaviour(new RequestResponseBehaviour(this, mt));

        // Setup finished. When the last inform is received, the agent itself will add
        // a behaviour to send/receive actions
        
        this.addBehaviour(new SimulationBehaviour(this, 100));
    }
    
    public int getMovableAgentsCount(){
        return this.moveableAgentsCount;
    }
    
    @Override
    public void takeDown(){
        //this.gui.dispose();
    }
    
    public void updateGUI() {
        this.gui.updateGame();
    }

}
