/**
 *  IMAS base code for the practical work.
 *  Copyright (C) 2014 DEIM - URV
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.urv.imas.agent;

import cat.urv.imas.behaviour.harvestersubcoordinator.AssignmentGarbageInitiator;
import cat.urv.imas.behaviour.harvestersubcoordinator.HarvesterSubCoordinatorBehaviour;
import cat.urv.imas.map.Cell;
import cat.urv.imas.onthology.GameSettings;
import cat.urv.imas.onthology.GarbageType;
import jade.core.*;
import jade.domain.FIPAAgentManagement.*;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import java.util.ArrayList;
import java.util.List;

/**
 * The Harvester Coordinator agent. 
 * TODO: This Coordinator is Coordinating Harvester Paper Agents, Harvester Glass 
 * Agent and HarvesterPlastic Agent, and communicate with Coordinator Agent.
 */
public class HarvesterSubCoordinatorAgent extends ImasAgent {

    /**
     * Game settings in use.
     */
    private GameSettings game;
    /**
     * Coordinator agent id.
     */
    public AID coordinatorAgent;
    /**
     * harvesters ids
     */
    public AID[] harvesters;
    /*
    * Garbage type for this coordinator
    */
    public GarbageType garbageType;
    /**
     * 
     */
    public List<Cell> buildingsWithGarbage;

    /**
     * Builds the harvester sub-coordinator agent.
     */
    public HarvesterSubCoordinatorAgent(AgentType type, GarbageType garbage) {
        super(type);
        this.garbageType = garbage;
        this.buildingsWithGarbage = new ArrayList<Cell>();
    }

    /**
     * Agent setup method - called when it first come on-line. Configuration of
     * language to use, ontology and initialization of behaviours.
     */
    @Override
    protected void setup() {
        Object[] arguments = getArguments();
        this.game = (GameSettings)arguments[0];
        
        /* ** Very Important Line (VIL) ***************************************/
        this.setEnabledO2ACommunication(true, 1);
        /* ********************************************************************/
        
        // Register the agent to the DF
        this.register();
        
        // search HarvesterAgent
        this.coordinatorAgent = this.search(AgentType.HARVESTER_COORDINATOR, null);

        //search for all harvesters
        ServiceDescription searchCriterion = new ServiceDescription();
        searchCriterion.setType(AgentType.HARVESTER.toString());
        
        this.harvesters = new AID[8];
        for(int i = 0;i < this.harvesters.length;i++){
            String agentName = "harvester"+(i+1);
            this.harvesters[i] = new AID(agentName, AID.ISLOCALNAME);
        }
        //this.harvesters = UtilsAgents.searchAgents(this, searchCriterion);
        
        
        //add behaviour
        this.addBehaviour(new HarvesterSubCoordinatorBehaviour(this, harvesters, garbageType));
        
        ACLMessage initialMessage = new ACLMessage(ACLMessage.CFP);
        initialMessage.setSender(this.getAID());
        initialMessage.clearAllReceiver();
        initialMessage.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
        this.addBehaviour(new AssignmentGarbageInitiator(this, initialMessage, harvesters));
    }

    /**
     * Update the game settings.
     *
     * @param game current game settings.
     */
    public void setGame(GameSettings game) {
        this.game = game;
    }

    /**
     * Gets the current game settings.
     *
     * @return the current game settings.
     */
    public GameSettings getGame() {
        return this.game;
    }
    
    /**
     * Gets garbage type for this coordinator.
     *
     * @return the garbage type for this coordinator.
     */
    public GarbageType getGarbageType() {
        return this.garbageType;
    }

}
