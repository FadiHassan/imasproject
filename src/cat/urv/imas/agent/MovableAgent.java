/**
 *  IMAS base code for the practical work.
 *  Copyright (C) 2014 DEIM - URV
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.urv.imas.agent;

import cat.urv.imas.map.BuildingCell;
import cat.urv.imas.map.Cell;
import cat.urv.imas.map.RecyclingCenterCell;
import cat.urv.imas.map.StreetCell;
import cat.urv.imas.onthology.Direction;
import cat.urv.imas.onthology.GameSettings;
import cat.urv.imas.onthology.InfoAgent;

/**
 * Agent abstraction used in this practical work.
 * It gathers common attributes and functionality from all agents.
 */
public class MovableAgent  extends ImasAgent {
    
    /**
     * Game settings in use.
     */
    private GameSettings game;
    /**
     * Information of this agent.
     */
    private InfoAgent infoAgent;
    /**
     * Current street cell for agent.
     */
    private StreetCell agentCell;
    
    /**
     * Creates the agent.
     * @param type type of agent to set.
     */
    public MovableAgent(AgentType type) {
        super(type);
    }
    
    /**
     * Agent setup method - called when it first come on-line. Configuration of
     * language to use, ontology and initialization of behaviours.
     */
    @Override
    protected void setup() {
        Object[] arguments = getArguments();
        this.game = (GameSettings)arguments[0];
        this.infoAgent = (InfoAgent)arguments[1];
        this.agentCell = (StreetCell)arguments[2];
        
        this.infoAgent.setAID(this.getAID());
        /* ** Very Important Line (VIL) ***************************************/
        this.setEnabledO2ACommunication(true, 1);
        /* ********************************************************************/

        // Register the agent to the DF
        this.register();
        
        
        //this.addBehaviour(new Behaviour1(this));
 
    }
    
    /**
     * generate new direction if there is obstacle in the road
     * @param dir current direction
     * @return new direction
     */
    public Direction nextDir(Direction dir){
        int d = dir.getValue();
        //Random r = new Random();
        //int newd;
        //while((newd = r.nextInt(4)) == d || newd % 2 == d % 2 || isBulding(Direction.values()[newd]));
        //return Direction.values()[newd];
        return Direction.values()[(d + 1) % 4];
    }
    
    /**
     * check if the agent can go to right road like real life car driving
     * @return the direction to insure the agent on the right road
     */
    /*
    public Direction checkRight(){
        Direction dir = this.getInfoAgent().getDirection();
        int currentDir = dir.getValue();
        int count = 0;
        for(int i = 0;i < 4;i++)
            if(!isBulding(Direction.values()[i]))
                count++;
            
        int newDir = (4 + currentDir - 1) % 4;
        Cell cell = getCell(Direction.values()[newDir]);        
        if(count == 4 && canMove(cell))
            return Direction.values()[newDir];
        else
            return dir;
    }*/
    
    
    private Cell[] lastPath = new Cell[10];
    private int cellCount = 0;
    public void savePath(Cell c){
        lastPath[cellCount++ % lastPath.length] = c;
    }
    
    public int numOfDir(Cell currentCell) {
        int count = 0;
        for(int i = 0;i < 4;i++){
            Cell cell = getCell(currentCell, Direction.values()[i]);
            if(!isBulding(cell))
                count++;
        }
        return count;
    }
    
    public int numOfDir(){
        int count = 0;
        for(int i = 0;i < 4;i++){
            Cell cell = getCell(Direction.values()[i]);
            if(!isBulding(cell))
                count++;
        }
        return count;
    }
    
    public Direction checkRight(){
        int currentDir = this.getInfoAgent().getDirection().getValue();
        int count = numOfDir();
            
        int newDir = (4 + currentDir - 1) % 4;
        Cell cell = getCell(Direction.values()[newDir]);        
        if(count == 4 && canMove(cell))
            return Direction.values()[newDir];
        else if(count <= 3 && !UtilsAgents.isContains(lastPath, cell) && canMove(cell))
            return Direction.values()[newDir];
        else
            return this.getInfoAgent().getDirection();
    }
    
    /**
     * get the cell on next step with specific direction
     * @param d the direction
     * @return the cell in this direction
     */
    public Cell getCell(Direction d){
        return getCell(this.agentCell, d);
    }
    
    /**
     * get the cell on next step with specific direction
     * @param currentCell current cell
     * @param d the direction
     * @return the cell in this direction
     */
    public Cell getCell(Cell currentCell, Direction d){
        int i = 0, j = 0;
        switch(d){
            case RIGHT:
                i = 0; j = 1;
                break;
            case UP:
                i = -1; j = 0;
                break;
            case LEFT:
                i = 0; j = -1;
                break;
            case DOWN:
                i = 1; j = 0;
                break;
        }
        return this.game.get(currentCell.getRow() + i, currentCell.getCol() + j);
    }
    
    
    /**
     * check if there is a building in this direction 
     * @param d the direction
     * @return true if there is building in this direction, otherwise return false
     */
    public boolean isBulding(Direction d){
        Cell cell = getCell(d);
        return isBulding(cell);
    }
    
    /**
     * check if there is a building in this direction 
     * @param cell the map cell
     * @return true if there is building in this cell, otherwise return false
     */
    public boolean isBulding(Cell cell){
        return cell instanceof BuildingCell || cell instanceof RecyclingCenterCell;
    }
    
    /**
     * initializing agent direction
     */
    public void init(){
        Direction dir = Direction.NONE;
        for(int i = 0;i < 4;i++)
            if(this.isBulding(Direction.values()[i])){
                dir = Direction.values()[((i + 1) % 4)];
                break;
            }
        if(dir == Direction.NONE)
            dir = Direction.RIGHT;
        this.getInfoAgent().setDirection(dir);
    }
    
    /**
     * check if the agent can move to specific cell
     * @param cell the specific cell we want to check it
     * @return true if the agent can move to current direction, otherwise return false
     */
    public boolean canMove(Cell cell){
        if(cell instanceof StreetCell && !((StreetCell)cell).isThereAnAgent())
            return true;
        return false;
    }
    
    /**
     * check if the agent can move in the current direction
     * @return true if the agent can move to current direction, otherwise return false
     */
    public boolean canMove(){
        Direction dir = this.getInfoAgent().getDirection();
        return canMove(getCell(dir));
    }
    
    /**
     * Move the agent one step per turn
     * @param currentAgent the agent want to move
     * @return true if the agent moved successfully 
     */
    public static synchronized boolean move(MovableAgent currentAgent){
        try{
            Direction dir = currentAgent.getInfoAgent().getDirection();
            Cell cell = currentAgent.getCell(dir);  
            if(cell instanceof StreetCell && !((StreetCell)cell).isThereAnAgent()){
                currentAgent.agentCell.removeAgent(currentAgent.infoAgent);
                currentAgent.agentCell = ((StreetCell)cell);
                currentAgent.agentCell.addAgent(currentAgent.infoAgent);
            }
            else
                return false;
        }
        catch(Exception ex)
        {
            currentAgent.log(ex.toString());
        }
        return true;
    }
    /*
    public static synchronized boolean move(MovableAgent agent, Cell cell){
        try{
            if(cell instanceof StreetCell && !((StreetCell)cell).isThereAnAgent()){
                agent.getAgentCell().removeAgent(agent.infoAgent);
                agent.setAgentCell(((StreetCell)cell));
                agent.getAgentCell().addAgent(agent.infoAgent);
            }
            else
                return false;
        }
        catch(Exception ex)
        {
            agent.log(ex.toString());
        }
        return true;
    }
    */
    /**
     * Update the game settings.
     *
     * @param game current game settings.
     */
    public void setGame(GameSettings game) {
        this.game = game;
    }

    /**
     * Gets the current game settings.
     *
     * @return the current game settings.
     */
    public GameSettings getGame() {
        return this.game;
    }
    
    /* ********************************************************************** */
    /**
     * set the current agent info.
     *
     * @param info current agent info.
     */
    public void setInfoAgent(InfoAgent info) {
        this.infoAgent = info;
    }
    
    /**
     * Gets the current agent info.
     *
     * @return the current agent info.
     */
    public InfoAgent getInfoAgent() {
        return this.infoAgent;
    }
    
    /**
     * set the current agent cell.
     *
     * @param cell current agent cell.
     */
    public void setAgentCell(StreetCell cell) {
        this.agentCell = cell;
    }
    
    /**
     * Gets the current agent cell.
     *
     * @return the current agent cell.
     */
    public StreetCell getAgentCell() {
        return this.agentCell;
    }

    /* ********************************************************************** */
    /**
     * Gets a string representation of the cell.
     *
     * @return
     */
    @Override
    public String toString() {
        String str = "(Agent-name " + this.getAID() + " "
                + "(r " + this.agentCell.getRow() + ")"
                + "(c " + this.agentCell.getCol() + ")";
        return str + ")";
    }
}
