/**
 *  IMAS base code for the practical work.
 *  Copyright (C) 2014 DEIM - URV
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.urv.imas.agent;

import cat.urv.imas.behaviour.scout.ScoutBehaviour;
import cat.urv.imas.onthology.ScoutPGP;
import jade.core.AID;

/**
 * The Scout agent. 
 * TODO: Agent responsibilities: can bring several types of garbage, 
 * but only a single type of garbage at a time.
 */
public class ScoutAgent extends MovableAgent {

    /**
     * Scout Coordinator agent id.
     */
    public AID scoutCoordinatorAgent;
    /**
     * partial global plan
     */
    public ScoutPGP pgp;
    /**
     * Builds the scout agent.
     */
    public ScoutAgent() {
        super(AgentType.SCOUT);
    }
    
    /**
     * Agent setup method - called when it first come on-line. Configuration of
     * language to use, ontology and initialization of behaviours.
     */
    @Override
    protected void setup() {
        super.setup();
        
        this.pgp = (ScoutPGP)getArguments()[3];
        
        // search Scout Coordinator Agent
        this.scoutCoordinatorAgent = this.search(AgentType.SCOUT_COORDINATOR, null);
        
        this.addBehaviour(new ScoutBehaviour(this));
    }
}
