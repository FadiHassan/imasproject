/**
 *  IMAS base code for the practical work.
 *  Copyright (C) 2014 DEIM - URV
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.urv.imas.agent;

import cat.urv.imas.behaviour.harvestorcoordinator.HarvesterCoordinatorBehaviour;
import cat.urv.imas.onthology.GameSettings;
import jade.core.*;
import jade.domain.*;
import jade.domain.FIPAAgentManagement.*;

/**
 * The Harvester Coordinator agent. 
 * TODO: This Coordinator is Coordinating Harvester Paper Agents, Harvester Glass 
 * Agent and HarvesterPlastic Agent, and communicate with Coordinator Agent.
 */
public class HarvesterCoordinatorAgent extends ImasAgent {

    /**
     * Game settings in use.
     */
    private GameSettings game;
    /**
     * Coordinator agent id.
     */
    public AID coordinatorAgent;
     /**
     * Harvester Paper Coordinator agent id.
     */
    public AID harvesterPaperCoordinatorAgent;
    /**
     * Harvester Glass Coordinator agent id.
     */
    public AID harvesterGlassCoordinatorAgent;
    /**
     * Harvester Plastic Coordinator agent id.
     */
    public AID harvesterPlasticCoordinatorAgent;

    /**
     * Builds the harvester coordinator agent.
     */
    public HarvesterCoordinatorAgent() {
        super(AgentType.HARVESTER_COORDINATOR);
    }

    /**
     * Agent setup method - called when it first come on-line. Configuration of
     * language to use, ontology and initialization of behaviours.
     */
    @Override
    protected void setup() {
        Object[] arguments = getArguments();
        this.game = (GameSettings)arguments[0];
        
        /* ** Very Important Line (VIL) ***************************************/
        this.setEnabledO2ACommunication(true, 1);
        /* ********************************************************************/

        // Register the agent to the DF
        this.register();
        
        // search SystemAgent
        this.coordinatorAgent = this.search(AgentType.COORDINATOR, null);
        
        // search Harvester Paper Coordinator Agent
        this.harvesterPaperCoordinatorAgent = this.search(AgentType.HARVESTER_PAPER_COORDINATOR, null);
        
        // search Harvester Paper Coordinator Agent
        this.harvesterGlassCoordinatorAgent = this.search(AgentType.HARVESTER_GLASS_COORDINATOR, null);
        
        // search Harvester Plastic Coordinator Agent
        this.harvesterPlasticCoordinatorAgent = this.search(AgentType.HARVESTER_PLASTIC_COORDINATOR, null);
        
        this.addBehaviour(new HarvesterCoordinatorBehaviour(this));
        
    }

    /**
     * Update the game settings.
     *
     * @param game current game settings.
     */
    public void setGame(GameSettings game) {
        this.game = game;
    }

    /**
     * Gets the current game settings.
     *
     * @return the current game settings.
     */
    public GameSettings getGame() {
        return this.game;
    }

}
