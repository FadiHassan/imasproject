/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.onthology;

/**
 *
 * @author Fadi
 */
public interface CollectGarbageInTheCityVocabulary {
    // VOCABULARY
    public static final String GARBAGE = "Garbage";
    public static final String GARBAGE_TYPE = "type";
    public static final String GARBAGE_AMOUNT = "amount";
    public static final String GARBAGE_PRICE = "price";
    public static final String RECYCLE_CENTER = "Recyclecenter";
    public static final String RECYCLE_LOCATION = "location";
    public static final String RECYCLE_PRICES = "prices";
}
