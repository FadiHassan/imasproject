/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.onthology;

import cat.urv.imas.agent.ScoutAgent;
import cat.urv.imas.map.BuildingCell;
import cat.urv.imas.map.Cell;

/**
 *
 * @author Fadi
 */
public class ScoutPGP {
    private Cell[][] map = null;
    private int[][] pgp = null;
    private int maxStep;
    public ScoutPGP(Cell[][] map, int maxStep){
        this.map = map;
        this.maxStep = maxStep;
        init();
    }
    
    public void init(){
        this.pgp = new int[this.map.length][];
        for(int i = 0;i < this.pgp.length;i++){
            this.pgp[i] = new int[this.map[i].length];
            for(int j = 0;j < this.pgp[i].length;j++){
                if(this.map[i][j] instanceof BuildingCell){
                    //BuildingCell cell = (BuildingCell)this.map[i][j];
                    this.pgp[i][j] = this.maxStep;
                }
                else{
                    this.pgp[i][j] = -1;
                }
            }
        }
    }
    
    public Direction getNextDirection(ScoutAgent agent){
        int[] v = new int[4];
        int min = agent.checkRight().getValue();
        for(int i = 0;i < 4;i++){
            Cell cell = agent.getCell(Direction.values()[i]);
            v[i] = getValue(cell.getRow(), cell.getCol());
            if(v[i] != -1 && v[i] < min)
                min = v[i];
        }
        return Direction.values()[min];
    }
    
    public void setValue(int i, int j, int value){
        this.pgp[i][j] = value;
    }
    
    public int getValue(int i, int j){
        return this.pgp[i][j];
    }
}
