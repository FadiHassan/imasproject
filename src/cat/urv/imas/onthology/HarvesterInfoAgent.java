/**
 * IMAS base code for the practical work. 
 * Copyright (C) 2014 DEIM - URV
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cat.urv.imas.onthology;

import cat.urv.imas.agent.AgentType;
import cat.urv.imas.map.BuildingCell;
import jade.core.AID;
import java.util.HashMap;
import java.util.Map;

/**
 * Agent information for harvester agents.
 */
public class HarvesterInfoAgent extends InfoAgent {

    /**
     * Types of garbage allowed to harvest.
     */
    protected GarbageType[] allowedTypes;
    /**
     * Maximum units of garbage of any type able to harvest at a time.
     */
    protected int capacity;
    /**
     * Garbage of the agent: it can only be of one type at a time.
     * But, when the agent harvest the garbage, it can be of any allowed type and any amount.
     */
    protected Map<GarbageType, Integer> garbage;
    
    public HarvesterInfoAgent(AgentType type) {
        super(type);
        garbage = new HashMap();
    }
    
    public HarvesterInfoAgent(AgentType type, GarbageType[] allowedTypes, int capacity) {
        super(type);
        this.allowedTypes = allowedTypes;
        this.capacity = capacity;
        garbage = new HashMap();
        for(GarbageType g: allowedTypes)
            garbage.put(g, 0);
    }
    
    public HarvesterInfoAgent(AgentType type, AID aid) {
        super(type, aid);
        garbage = new HashMap();
    }
    
    public HarvesterInfoAgent(AgentType type, AID aid, GarbageType[] allowedTypes, int capacity) {
        super(type, aid);
        this.allowedTypes = allowedTypes;
        this.capacity = capacity;
        garbage = new HashMap();
        for(GarbageType g: allowedTypes)
            garbage.put(g, 0);
    }
    
    /*
    * add 1 unit of garbage to harvester container
    */
    public void addGarbage(GarbageType type){
        garbage.putIfAbsent(type, 0);
        for (Map.Entry<GarbageType, Integer> entry : this.garbage.entrySet()){
            if(entry.getKey() == type)
                entry.setValue(entry.getValue() + 1);
        }
    }
    
    /**
     * remove 1 unit of garbage to harvester container
    */
    public void removeGarbage(GarbageType type){
        for (Map.Entry<GarbageType, Integer> entry : this.garbage.entrySet()){
            if(entry.getKey() == type)
                entry.setValue(entry.getValue() - 1);
        }
    }
    
    /**
     * get garbage founded in harvester container
    * @return current garbage in the harvester container
    */
    public Map.Entry<GarbageType, Integer> getGarbage(){
        for(Map.Entry m:this.garbage.entrySet()){
            if(Integer.parseInt(m.getValue()+"") > 0){
                return m;
            }
        }
        return null;
    }
    
    /**
     * Gets capacity for harvester.
     *
     * @return harvester capacity.
     */
    public int getCapacity() {
        return this.capacity;
    }
    
    /**
     * get Garbage Type Allowed.
     *
     * @return array of allowed garbages.
     */
    public GarbageType[] getAllowedType(){
        return this.allowedTypes;
    }
    
    /**
     * check if spacific garbage type allowed or the container of the harvester have the same garbage type
     * @param type of garbage to check
     * @return true if the garbage type is allowed, otherwise return false
     */
    public boolean isAllowdGarbageType(GarbageType type){
        boolean allowed = false;
        for (GarbageType entry : this.allowedTypes){
            if(entry.equals(type))
                allowed = true;
        }
        if(allowed){
            for (Map.Entry<GarbageType, Integer> entry : this.garbage.entrySet()){
                if(!entry.getKey().equals(type) && entry.getValue() > 0)
                    allowed = false;
            }
        }
        return allowed;
    }
    
    /**
     * String representation of this isntance.
     *
     * @return string representation.
     */
    @Override
    public String toString() {
        boolean[] allowed = new boolean[]{false, false, false};
        for (GarbageType g : allowedTypes) {
            switch (g) {
                case GLASS: 
                    allowed[0] = true;
                    break;
                case PAPER:
                    allowed[1] = true;
                    break;
                case PLASTIC:
                    allowed[2] = true;
                    break;
            }
        }
        String[] names = new String[] {"glass", "paper", "plastic"}; 
        String types = "";
        for (int i=0; i < allowed.length; i++) {
            if (allowed[i]) {
                types += " " +names[i];
            }
        }
        if (types.length() > 0) {
            types = "(allowed-garbage " + types + ")";
        }
        return "(info-agent (agent-type " + this.getType() + ")"
                + ((null != this.getAID()) ? (" (aid " + this.getAID() + ")") : "")
                + " (capacity " + capacity + ")"
                + " " + types
                + ")";
    }
    
    @Override
    public String getMapMessage() {
        String types = "";
        for (GarbageType g: allowedTypes) {
            types += "," + g.getShortString() + ((Integer.parseInt(garbage.get(g)+"") > 0)? "("+garbage.get(g).toString()+")":"");
        }
        return super.getMapMessage() +  ": " + types.substring(1);
    }

}
