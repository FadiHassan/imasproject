/**
 * IMAS base code for the practical work. 
 * Copyright (C) 2016 DEIM - URV
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cat.urv.imas.onthology;

/**
 * agent direction.
 * It provides a way of representing the direction of an agent.
 */
public enum Direction {
    RIGHT(0) {
        @Override
        public String getShortString() {
            return "→";
        }
    },
    UP (1){
        @Override
        public String getShortString() {
            return "↑";
        }
    },
    LEFT (2){
        @Override
        public String getShortString() {
            return "←";
        }
    },
    DOWN (3){
        @Override
        public String getShortString() {
            return "↓";
        }
    },
    NONE(-1){
        @Override
        public String getShortString() {
            return "∞";
        }
    };
    
    private final int value;

    private Direction(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
    
    public abstract String getShortString();
}
