/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.onthology;

import static examples.content.eco.elements.Costs.ITEM;
import jade.content.onto.BasicOntology;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.schema.AgentActionSchema;
import jade.content.schema.ConceptSchema;
import jade.content.schema.ObjectSchema;
import jade.content.schema.PredicateSchema;
import jade.content.schema.PrimitiveSchema;

/**
 *
 * @author Fadi
 */
public class CollectGarbageInTheCityOntology extends Ontology implements CollectGarbageInTheCityVocabulary {
    // The name identifying this ontology
    public static final String ONTOLOGY_NAME = "Collect-garbage-in-the-city";
        
    // The singleton instance of this ontology
    private static Ontology theInstance = new CollectGarbageInTheCityOntology();
    // Retrieve the singleton Collect-garbage-in-the-city ontology ontology instance
    public static Ontology getInstance() {
        return theInstance;
    }
    
    // Private constructor
    private CollectGarbageInTheCityOntology() {
        // The Collect-garbage-in-the-city ontology extends the basic ontology
        super(ONTOLOGY_NAME, BasicOntology.getInstance());
        try {
            //add(new ConceptSchema(BOOK), Book.class);
            //add(new PredicateSchema(COSTS), Costs.class);
            //add(new AgentActionSchema(SELL), Sell.class);
            
            // Structure of the schema for the Book concept
            ConceptSchema cs = (ConceptSchema) getSchema(ITEM);
            //cs.add(BOOK_TITLE, (PrimitiveSchema)
            //getSchema(BasicOntology.STRING));
            //cs.add(BOOK_AUTHORS, (PrimitiveSchema)
            //getSchema(BasicOntology.STRING), 0,
            //ObjectSchema.UNLIMITED);
            //cs.add(BOOK_EDITOR, (PrimitiveSchema)
            //getSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
            // Structure of the schema for the Costs predicate
            // Structure of the schema for the Sell agent action
            //AgentActionSchema as = (AgentActionSchema) getSchema(SELL);
            //as.add(SELL_ITEM, (ConceptSchema) getSchema(BOOK));
        }
        catch (OntologyException oe) {
            oe.printStackTrace();
        }
    }
}
