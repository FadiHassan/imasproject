/**
 *  IMAS base code for the practical work.
 *  Copyright (C) 2014 DEIM - URV
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.urv.imas.onthology;

/**
 * Content messages for inter-agent communication.
 */
public class MessageContent {
    
    /**
     * Message sent from Coordinator agent to System agent to get the whole
     * city information.
     */
    public static final String GET_MAP = "Get map";
    /**
     * Message send from moveable agents to system agent to start simulation
     */
    public static final String I_AM_READY = "I am ready";
    /**
     * Message sent from Scout agent to ScoutCoordinator agent to get information
     * about PGP.
     */
    public static final String GET_DIRECTION = "Get direction";
    /**
     * Message sent from ScoutCoordinator agent to Scout agent to get information
     * about detected garbage in the city.
     */
    public static final String GET_DETECTED_GARBAGE = "Get detected garbage";
    /**
     * Ask the sub-coordinators about nearest garbage in the city
     */
    public static final String GET_NEAREST_GARBAGE = "Get nearest garbage";
    
    public static final String GLASS_GARBAGE = "Glass garbage";
    public static final String PAPER_GARBAGE = "Paper garbage";
    public static final String PLASTIC_GARBAGE = "Plastic garbage";
    
}
