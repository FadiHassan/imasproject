/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.astar;

/**
 *
 * @author Fadi
 */
public interface IPriorityQueue<TState> {
    double evaluatuionFunction(INode<TState> node, IProblem<TState> problem);
}
