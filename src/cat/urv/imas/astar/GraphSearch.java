/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.astar;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Fadi
 * @param <TState>
 */
public class GraphSearch<TState> extends Search<TState> {
    
    public ISolution<TState>[] searchMulti(IFringe<TState> fringe, int solutionsCount) {
        /*
         * function GRAPH-SEARCH(problem, fringe) return a solution, or failure
         *     closed ← an empty set
         *     fringe ← INSERT(MAKE-NODE(INITIAL-STATE[problem], fringe))
         *     loop do
         *     if EMPTY?(fringe) then 
         *         return failure
         *     node ←  REMOVE-FIRST(fringe)
         *     if GOAL-TEST[problem] applied to STATE[node] succeeds then
         *         return SOLUTION(node)
         *     if STATE[node] is not in closed then
         *         add STATE[node] to closed
         *         fringe ← INSERT-ALL(EXPAND(problem) , fringe)
         */
        //save start time
        this.startTime = Instant.now();

        List<ISolution<TState>> solutions = new ArrayList<ISolution<TState>>();
        ISolution<TState> solution = null;
        fringe.addInitialState();
        do
        {
            if (fringe.isEmpty())
                break;
            INode<TState> node = fringe.removeFirst();

            //event
            //OnSelectedNodeChanged(new NodeEventArgs<TState>(node));
            //increment expanded nodes
            this.expandedNodes++;
            //stop if time is out
            if (this.timeout > 0 && (Duration.between(this.startTime, Instant.now()).toMillis() / 1000) > this.timeout)
                break;

            if (fringe.getProblem().goalTest(node.getState()))
            {
                solution = new Solution<TState>(node, node.getPathToGoal());
                if (!solutions.contains(solution))
                    solutions.add(solution);
                if (solutions.size() >= solutionsCount)
                    break;
            }
            if (!fringe.getClosedList().contains(node))
            {
                fringe.getClosedList().add(node);
                fringe.expand(node);
            }
        }
        while (true);

        //save stop time
        this.stopTime = Instant.now();
        //compute time
        time =  Duration.between(this.startTime, this.stopTime).toMillis() / 1000;
        //compute total nodes expanded
        this.allNodes = this.expandedNodes + fringe.size();
        ISolution[] array = new ISolution[solutions.size()];
        array = solutions.toArray(array);
        return array;
    }
}
