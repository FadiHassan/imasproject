/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.astar;

/// <summary>
    /// تعريف واجهة العقدة
    /// </summary>
    /// <typeparam name="TState">للمشكلة المراد حلها  (state) اسم الصنف المتضمن الحالة </typeparam>
/**
 * Interface of the tree node
 * @author Fadi
 * @param <TState> state in the current node
 */
public interface INode<TState> {
    public TState getState();
    public INode<TState> getParent();
    public int getAction();
    public double getPathCost();
    public void setPathCost(double cost);
    public int getDepth();
    public void setDepth(int depth);
    public double getStepCost();
    public void setStepCost(double cost);
    public boolean isRootNode();
    public INode<TState>[] getPathFromGoal();
    public INode<TState>[] getPathToGoal();
}
