/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.astar;

/**
 *
 * @author Fadi
 */
public class Problem<TState> implements IProblem<TState> {
    private TState initialState;
    private IProblemFunctions<TState> problemFunctions;
    
    public Problem(IProblemFunctions<TState> problemFunctions) {
        this.problemFunctions = problemFunctions;
    }

    @Override
    public TState getInitialState() {
        return this.initialState;
    }

    @Override
    public void setInitialState(TState state) {
        this.initialState = state;
    }
    
    @Override
    public double stepCostFunction(TState fromState, TState toState, int action) {
        return problemFunctions.stepCostFunction(fromState, toState, action);
    }
    
    @Override
    public double heuristicFunction(TState state) {
        return problemFunctions.heuristicFunction(state);
    }

    @Override
    public boolean goalTest(TState state) {
        return problemFunctions.goalTest(state);
    }

    @Override
    public ISuccessor<TState>[] successorFunction(TState state) {
        return problemFunctions.successorFunction(state);
    }

    @Override
    public IProblemFunctions<TState> getProblemFunctions() {
        return this.problemFunctions;
    }

    @Override
    public void setProblemFunctions(IProblemFunctions<TState> problemFunctions) {
        this.problemFunctions = problemFunctions;
    }
}
