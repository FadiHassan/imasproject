/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.astar;

import java.util.List;

/**
 *
 * @author Fadi
 */
public interface IFringe<TState> {
    public IProblem<TState> getProblem();
    public void setProblem(IProblem<TState> problem);
    public void addInitialState();
    public void add(INode<TState> node);
    public void addRange(INode<TState>[] nodes);
    public INode<TState>[] expand(INode<TState> node);
    public INode<TState> first();
    public INode<TState> removeFirst();
    public List<INode<TState>> getOpenedList();
    public List<INode<TState>> getClosedList();
    public boolean isEmpty();
    public int size();
 }
