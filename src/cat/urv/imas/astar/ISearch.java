/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.astar;

/**
 *
 * @author Fadi
 */
public interface ISearch<TState> {
    public ISolution<TState> searchOne(IFringe<TState> fringe);
    public ISolution<TState>[] searchMulti(IFringe<TState> fringe, int solutionsCount);
    public ISolution<TState>[] searchAll(IFringe<TState> fringe);
    //event NodeHandler<TState> SelectedNodeChanged;
    public double getTime();
    public double getTimeout();
    public void setTimeout(double  timout);
    public double getAllNodes();
    public double getExpandedNodes();
}
