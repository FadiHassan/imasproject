/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.astar;

/**
 *
 * @author Fadi
 */
public class Solution<TState> implements ISolution<TState> {
    private INode<TState> goal;
    @Override
    public INode<TState> getGoalNode() {
        return goal;
    }
    
    private INode<TState>[] goalPath = null;
    @Override
    public INode<TState>[] getGoalPath() {
        return this.goalPath;
    }
    
    public Solution(INode<TState> state, INode<TState>[] path) {
        this.goal = state;
        this.goalPath = path;
    }
    
    @Override
    public boolean equals(Object obj) {
        boolean f = false;
        if (obj instanceof Solution) {
            Solution<TState> s = (Solution<TState>)obj;
            if (this.getGoalNode().equals(s.getGoalNode())
                && this.getGoalPath().length == s.getGoalPath().length) {
                f = true;
                for (int i = 0; i < this.getGoalPath().length && f; i++)
                    f = (this.getGoalPath()[i].equals(s.getGoalPath()[i]));
            }
        }
        return f;
    }
}
