/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.astar;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Fadi
 */
public class TreeSearch<TState> extends Search<TState> {
    public ISolution<TState>[] searchMulti(IFringe<TState> fringe, int solutionsCount) {
        //save start time
        this.startTime = Instant.now();

        List<ISolution<TState>> solutions = new ArrayList<ISolution<TState>>() {};
        ISolution<TState> solution = null;
        //fringe <- INSERT(MAKE-NODE(INITIAL-STATE[problem]), fringe)
        fringe.addInitialState();
        //loop do
        do
        {
            //if EMPTY?(fringe) then return failure
            if (fringe.isEmpty())
                break;
            //node <- REMOVE-FIRST(fringe)
            INode<TState> node = fringe.removeFirst();
            //if GOAL-TEST[problem] applied to STATE[node] succeeds

            //event
            //OnSelectedNodeChanged(new NodeEventArgs<TState>(node));
            //increment expanded nodes
            this.expandedNodes++;
            //stop if time is out
            if (this.timeout > 0 && (Duration.between(this.startTime, Instant.now()).toMillis() / 1000) > this.timeout)
                break;

            if (fringe.getProblem().goalTest(node.getState())) {
                //return SOLUTION(node)
                solution = new Solution<TState>(node, node.getPathToGoal());
                if (!solutions.contains(solution))
                    solutions.add(solution);
                if (solutions.size() >= solutionsCount)
                    break;
            }
            //fringe <- INSERT-ALL(EXPAND(node, problem), fringe)
            fringe.expand(node);
        }
        while (true);

        //save stop time
        this.stopTime = Instant.now();
        //compute time
        time =  Duration.between(this.startTime, this.stopTime).toMillis() / 1000;
        //compute total nodes expanded
        this.allNodes = this.expandedNodes + fringe.size();
        ISolution[] array = new ISolution[solutions.size()];
        array = solutions.toArray(array);
        return array;
    }
}
