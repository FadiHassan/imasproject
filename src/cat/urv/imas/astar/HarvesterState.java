/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.astar;

import cat.urv.imas.map.Cell;

/**
 *
 * @author Fadi
 */
public class HarvesterState extends State<Cell> {
    
    public HarvesterState(Cell cell){
        this.data = cell;
    }
    
    @Override
    public boolean equals(Object obj) {
        if(obj instanceof HarvesterState){
            HarvesterState s = (HarvesterState)obj;
            if(s != null)
                return this.data.equals(s.data);
        }
        return false;
    }
}
