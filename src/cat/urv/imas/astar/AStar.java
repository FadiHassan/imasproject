/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.astar;

/**
 *
 * @author Fadi
 */
public class AStar<TState> extends Fringe<TState> implements IPriorityQueue<TState> {
    public AStar(IProblem<TState> problem) {
        super(problem);
    }
    
    @Override
    public void add(INode<TState> node)
    {
        int i = 0;
        while (i < openedList.size() && evaluatuionFunction(node, getProblem()) > evaluatuionFunction(openedList.get(i), getProblem()))
            i++;
        while (i < openedList.size() && evaluatuionFunction(node, getProblem()) == evaluatuionFunction(openedList.get(i), getProblem()) && node.getPathCost() > openedList.get(i).getPathCost())
            i++;

        int index = closedList.indexOf(node);
        if (index != -1 && node.getPathCost() < closedList.get(index).getPathCost())
            closedList.remove(index);

        openedList.add(i, node);
    }
    
    @Override
    public double evaluatuionFunction(INode<TState> node, IProblem<TState> problem) {
        return node.getPathCost() + problem.heuristicFunction(node.getState());
    }
}
