/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.astar;

/**
 *
 * @author Fadi
 */
public interface IProblem<TState> extends IProblemFunctions<TState> {
    public TState getInitialState();
    public void setInitialState(TState state);
    public IProblemFunctions<TState> getProblemFunctions();
    public void setProblemFunctions(IProblemFunctions<TState> problemFunctions);
}
