/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.astar;

import java.time.Instant;

/**
 *
 * @author Fadi
 * @param <TState>
 */
public abstract class Search<TState> implements ISearch<TState> {
    @Override
    public ISolution<TState>[] searchAll(IFringe<TState> fringe) {
        return searchMulti(fringe, Integer.MAX_VALUE);
    }

    @Override
    public ISolution<TState> searchOne(IFringe<TState> fringe) {
        ISolution<TState>[] solutions = searchMulti(fringe, 1);
        if (solutions.length > 0)
            return solutions[0];
        return null;
    }
    
    @Override
    public abstract ISolution<TState>[] searchMulti(IFringe<TState> fringe, int solutionsCount);
    
    //public event NodeHandler<TState> SelectedNodeChanged;
    //protected virtual void OnSelectedNodeChanged(NodeEventArgs<TState> e) {
    //    if (SelectedNodeChanged != null)
    //        SelectedNodeChanged(this, e);
    //}
    protected Instant startTime;
    protected Instant stopTime;
    protected double time = 0;
    @Override
    public double getTime() { 
        return time; 
    }

    protected double timeout = -1;
    @Override
    public double getTimeout() {
        return this.timeout;
    }

    @Override
    public void setTimeout(double timout){
        this.timeout = timeout;
    }

    protected double expandedNodes = 0;
    @Override
    public double getExpandedNodes(){
        return this.expandedNodes; 
    }
    
    protected double allNodes = 0;
    @Override
    public double getAllNodes(){
        return this.allNodes; 
    }
}
