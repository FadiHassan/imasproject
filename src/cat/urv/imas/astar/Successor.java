/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.astar;

/**
 *
 * @author Fadi
 */
public class Successor<TState> implements ISuccessor<TState> {
    private int action;
    private TState state;
    
    public Successor(int action, TState state) {
        this.action = action;
        this.state = state;
    }

    @Override
    public int getAction() {
        return this.action;
    }

    @Override
    public void setAction(int action) {
        this.action = action;
    }

    @Override
    public TState getState() {
        return this.state;
    }

    @Override
    public void setState(TState state) {
        this.state = state;
    }
}
