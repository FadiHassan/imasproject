/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.astar;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Fadi
 */
public class Fringe<TState> implements IFringe<TState> {
    protected List<INode<TState>> openedList = new ArrayList<INode<TState>>();
    @Override
    public List<INode<TState>> getOpenedList() { return openedList; }

    protected List<INode<TState>> closedList = new ArrayList<INode<TState>>();
    @Override
    public List<INode<TState>> getClosedList() { return closedList; } 
    
    private IProblem<TState> problem;
    @Override
    public IProblem<TState> getProblem(){
        return problem;
    }
    
    @Override
    public void setProblem(IProblem<TState> problem){
        this.problem = this.problem;
    }

    public Fringe(IProblem<TState> problem) {
        this.problem = problem;
    }
    
    @Override
    public void addInitialState() {
        this.openedList.clear();
        this.closedList.clear();
        this.openedList.add(new Node<TState>(problem.getInitialState()));
    }
    
    @Override
    public void add(INode<TState> node) {
        if (this instanceof IPriorityQueue) {
            int i = 0;
            IPriorityQueue<TState> bestfirst = ((IPriorityQueue<TState>)this);
            while (i < openedList.size() && bestfirst.evaluatuionFunction(node, problem) > bestfirst.evaluatuionFunction(openedList.get(i), problem))
                i++;
            openedList.add(i, node);
        }
        else
            openedList.add(node);
    }
    
    @Override
    public void addRange(INode<TState>[] nodes) {
        for(INode<TState> node : nodes)
            add(node);
    }
    
    @Override
    public INode<TState>[] expand(INode<TState> node)
    {
        //function EXPAND(node, problem) returns a set of nodes
        //    successors <- the empty set
        //    for each <action,result> in SUCCESSOR-FN[problem](STATE[node])
        //        s <- a new NODE
        //        STATE[s] <- result
        //        PARENT-NODE[s] <- node
        //        ACTION[s] <- action
        //        PATH-COST[s] <- PATH-COST[node] + STEP-COST(STATE[node], action, result])
        //        DEPTH[s] = DEPTH[node] + 1
        //        add s to successors
        //    return successors
        List<INode<TState>> successors = new ArrayList<INode<TState>>();
        for(ISuccessor<TState> successor : problem.successorFunction(node.getState())) {
            INode<TState> s = new Node<TState>(node, successor.getState(), successor.getAction());
            s.setPathCost(node.getPathCost() + problem.stepCostFunction(node.getState(), s.getState(), s.getAction()));
            s.setDepth(node.getDepth() + 1);
            add(s);
            successors.add(s);
        }
        INode<TState>[] array = new INode[successors.size()];
        array = successors.toArray(array);
        return array;
    }
    
    @Override
    public INode<TState> first()
    {
        INode<TState> node = null;
        if (openedList.size() != 0) {
            node = openedList.get(0);
        }
        return node;
    }
    
    @Override
    public INode<TState> removeFirst() {
        INode<TState> node = first();
        if (node != null)
            openedList.remove(0);
        return node;
    }
    
    @Override
    public boolean isEmpty() {
        return openedList.size() == 0;
    }
    
    @Override
    public int size() { 
        return openedList.size();
    }
}
