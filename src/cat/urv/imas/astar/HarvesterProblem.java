/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.astar;

import cat.urv.imas.agent.HarvesterAgent;
import cat.urv.imas.map.Cell;
import cat.urv.imas.onthology.Direction;
import cat.urv.imas.onthology.GameSettings;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Fadi
 */
public class HarvesterProblem implements IProblemFunctions<HarvesterState> {
    private HarvesterState goal;
    private HarvesterAgent agent;
    private boolean checkArrond; 
    public HarvesterProblem(HarvesterState goal, HarvesterAgent agent, boolean checkArround){
        this.goal = goal;
        this.agent = agent;
        this.checkArrond = checkArround;
    }
    
    public HarvesterState getGoal(){
        return this.goal;
    }
    
    public void setGoal(HarvesterState goal){
        this.goal = goal;
    }
    
    public HarvesterAgent getAgent(){
        return this.agent;
    }
    
    public void setGame(HarvesterAgent agent){
        this.agent = agent;
    }

    @Override
    public boolean goalTest(HarvesterState state) {
        if(checkArrond)
            return this.goal.getData().manhattanDistance(state.getData()) < 2;
        return this.goal.equals(state);
    }

    @Override
    public double stepCostFunction(HarvesterState fromState, HarvesterState toState, int action) {
        return 1;
    }

    @Override
    public ISuccessor<HarvesterState>[] successorFunction(HarvesterState state) {
        List<ISuccessor> successors = new ArrayList<ISuccessor>();
        for(int i = 0;i < 4;i++){
            Direction d = Direction.values()[i];
            Cell cell = agent.getCell(state.data, d);
            if(!agent.isBulding(cell))
                successors.add(new Successor(i, new HarvesterState(cell)));
        }
        ISuccessor[] array = new ISuccessor[successors.size()];
        array = successors.toArray(array);
        return array;
    }

    @Override
    public double heuristicFunction(HarvesterState state) {
        //Manhattan distance
        return this.goal.getData().manhattanDistance(state.getData());
    }
}
