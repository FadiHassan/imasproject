/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.astar;

/**
 *
 * @author Fadi
 * @param <TState>
 */
public class DepthFirst<TState> extends Fringe<TState> {
    public DepthFirst(IProblem<TState> problem) {
        super(problem);
    }

    @Override
    public void add(INode<TState> node) {
        openedList.add(0, node);
    }
}
