/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.astar;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 *
 * @author Fadi
 */
public class Node<TState> implements INode<TState> {
    private TState state;
    private INode<TState> parent;
    private int action;
    private double pathCost;
    private int depth;
    private double stepCost;
    
    public Node(TState state) {
        this.state = state;
    }
    
    public Node(TState state, int action){
        this.state = state;
        this.action = action;
        this.depth = 0;
        this.stepCost = 1;
        this.pathCost = 0;
    }
    
    public Node(INode<TState> parent, TState state, int action){
        this(state, action);
        this.parent = parent;
    }

    public void addToPathCost(double stepCost){
        this.pathCost = parent.getPathCost() + stepCost;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof INode){
            INode n = (INode<TState>)obj;
            return this.getState().equals(n.getState());
        }
        return false;
    }

    @Override
    public String toString(){
        String s = "";
        if (action != -1)
            s += action + "\n";
        if (state != null)
            s += state.toString();
        return s;
    }
    
    @Override
    public TState getState() {
        return this.state;
    }

    @Override
    public INode<TState> getParent() {
        return this.parent;
    }

    @Override
    public int getAction() {
        return this.action;
    }

    @Override
    public double getPathCost() {
        return this.pathCost;
    }

    @Override
    public void setPathCost(double cost) {
        this.pathCost = cost;
    }

    @Override
    public int getDepth() {
        return this.depth;
    }

    @Override
    public void setDepth(int depth) {
        this.depth = depth;
    }

    @Override
    public double getStepCost() {
        return this.stepCost;
    }

    @Override
    public void setStepCost(double cost) {
        this.stepCost = cost;
    }

    @Override
    public boolean isRootNode() {
        return parent == null;
    }

    @Override
    public INode<TState>[] getPathFromGoal() {
        INode<TState> current = this;
        Stack<INode<TState>> stack = new Stack<INode<TState>>();
        while (!current.isRootNode()){
            stack.push(current);
            current = current.getParent();
        }
        stack.push(current); // take care of root node
        INode<TState>[] array = new INode[stack.size()];
        array = stack.toArray(array);
        return array;
    }
    
    @Override
    public INode<TState>[] getPathToGoal() {
        INode<TState> current = this;
        List<INode<TState>> list = new ArrayList<INode<TState>>();
        while (!current.isRootNode()){
            list.add(current);
            current = current.getParent();
        }
        list.add(current); // take care of root node
        INode<TState>[] array = new INode[list.size()];
        for(int i = 0;i < array.length;i++)
            array[i] = list.get(list.size() - i - 1);
        return array;
    }
}
