/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.astar;

/**
 *
 * @author Fadi
 */
public class GreadyBestFirst<TState> extends Fringe<TState> implements IPriorityQueue<TState> {
        public GreadyBestFirst(IProblem<TState> problem) {
            super(problem);
        }
        public double evaluatuionFunction(INode<TState> node, IProblem<TState> problem) {
            return problem.heuristicFunction(node.getState());
        }
    }
