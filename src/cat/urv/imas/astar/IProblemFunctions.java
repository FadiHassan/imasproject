/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.astar;

/**
 *
 * @author Fadi
 * @param <TState>
 */
public interface IProblemFunctions<TState> {
    public boolean goalTest(TState state);
    public double stepCostFunction(TState fromState, TState toState, int action);
    public ISuccessor<TState>[] successorFunction(TState state);
    public double heuristicFunction(TState state);
}
