/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.astar;

/**
 *
 * @author Fadi
 */
public abstract class State<TData> implements IState<TData> {
    protected TData data;
    public TData getData(){
        return this.data;
    }

    public State() { }

    public State(TData data) {
        this.data = data;
    }
    
    public abstract boolean equals(Object obj);
}
