/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.astar;

/**
 *
 * @author Fadi
 */
public class BreadthFirst<TState> extends Fringe<TState> {
    public BreadthFirst(IProblem<TState> problem){
        super(problem);
    }
    
    public void Add(INode<TState> node){
        openedList.add(node);
    }
}
